Name:    asciidoc
Version: 10.2.1
Release: 1
Summary: Python 3 port of Asciidoc Python.
License: GPL-2.0-or-later
URL:     https://asciidoc-py.github.io/
Source0: https://github.com/asciidoc-py/asciidoc-py/releases/download/%{version}/%{name}-%{version}.tar.gz

BuildRequires: python3-devel docbook-style-xsl graphviz libxslt source-highlight
BuildRequires: texlive-dvisvgm-bin vim-filesystem symlinks automake autoconf
BuildRequires: fdupes python3-pytest python3-pytest-mock dblatex python3-pip  python3-wheel

Requires: python3 docbook-style-xsl graphviz libxslt source-highlight vim-filesystem
Requires: %{name} = %{version}-%{release}

Obsoletes: %{name}-latex < %{version}-%{release} %{name}-music < %{version}-%{release}
Provides: %{name}-latex = %{version}-%{release} %{name}-music = %{version}-%{release}

BuildArch: noarch

%description
Python 3 port of Asciidoc Python, this is a development work in progress,
but passes all tests, real world testing is very much welcomed.

%package_help
Requires: %{name} = %{version}-%{release}

Obsoletes: %{name}-doc < %{version}-%{release}
Provides: %{name}-doc = %{version}-%{release}

%prep
%autosetup -n %{name}-%{version}

%build
autoreconf -v
%configure
%make_build

%install
make install docs manpages DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_mandir}/man1/
mv %{buildroot}/share/doc/doc/{asciidoc.1,a2x.1,testasciidoc.1} %{buildroot}%{_mandir}/man1/
mkdir -p %{buildroot}/%{_pkgdocdir}/doc
mv %{buildroot}/share/doc/doc/ %{buildroot}/%{_pkgdocdir}/doc
mkdir -p %{buildroot}/%{_pkgdocdir}/doc/images
mv %{buildroot}/share/doc/images/ %{buildroot}/%{_pkgdocdir}/doc/images
rm  %{buildroot}/share/doc/{BUGS.adoc,CHANGELOG.adoc,INSTALL.adoc,README.md,dblatex/dblatex-readme.txt,docbook-xsl/asciidoc-docbook-xsl.txt}

%check
export PYTHONPATH="$PYTHONPATH:%{buildroot}%{python3_sitelib}"
export PYTHONDONTWRITEBYTECODE=1
python3 -m asciidoc.asciidoc --doctest
python3 -m pytest --ignore=_build.python3 -v
python3 tests/testasciidoc.py run

%files
%doc README.md BUGS.adoc CHANGELOG.adoc COPYRIGHT
%license COPYRIGHT LICENSE
%{_bindir}/a2x
%{_bindir}/%{name}
%{python3_sitelib}/
%exclude %{python3_sitelib}/asciidoc/resources/filters/latex
%exclude %{python3_sitelib}/asciidoc/resources/filters/music
%exclude %{_pkgdocdir}/doc
%dir %{python3_sitelib}/asciidoc/resources/filters/latex

%files help
%doc %{_mandir}/man1/
%doc doc

%changelog
* Thu Jul 25 2024 Funda Wang <fundawang@yeah.net> - 10.2.1-1
- update to 10.2.1

* Tue Mar 21 2023 lilong <lilong@kylinos.cn> - 10.2.0-1
- Upgrade to 10.2.0

* Fri Feb  3 2023 dillon chen <dillon.chen@gmail.com> - 10.1.4-1
- upgrade to 10.1.4

* Wed Feb  1 2023 dillon chen <dillon.chen@gmail.com> - 9.1.1-1
- upgrade to 9.1.1

* Tue Jun 14 2022 dillon chen <dillon.chen@gmail.com> - 9.1.0-1
- upgrade to 9.1.0

* Thu Nov 12 2020 shixuantong <shixuantong@huawei.com> - 9.0.4-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:Update to latest upstream release 9.0.4 and update Source0

* Mon Oct 28 2019 caomeng <caomeng5@huawei.com> - 8.6.10-3
- Type:NA
- ID:NA
- SUG:NA
- DESC:remove build requirement ImageMagick

* Mon Oct 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 8.6.10-2
- Package rebuild.

* Mon Sep 09 2019 openEuler Buildteam <buildteam@openeuler.org> - 8.6.10-1
- Package init.
